package main

import (
    "net/http"
)

func main() {
    conf := getConfig()
    fs := http.FileServer(http.Dir("web"))
    http.Handle("/", fs) // index.html
    http.HandleFunc("/mypass", makeUnipass) // handle request of making unipass 
    err := http.ListenAndServe(":" + conf.ListeningPort, nil) // set listen port
    if err != nil { panic(err) }
}